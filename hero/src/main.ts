import { AppModule } from './app.module';
import { bootstrap } from './microservice';
/**
 * Bootstrap with microservice
 */
bootstrap(AppModule);
