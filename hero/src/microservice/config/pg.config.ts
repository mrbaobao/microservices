import { PoolConfig } from "pg"
const PgConfig: PoolConfig = {
    "database": process.env.PG_DATABASE,
    "user": process.env.PG_USER ?? "postgres",
    "password": process.env.PG_PASSWORD,
    "host": process.env.PG_HOST,
    "port": +process.env.PG_PORT,
    "ssl": false,
    "max": 100,
    "min": 0,
    "idleTimeoutMillis": 1000
}
//Connection name
const ConnectionName = process.env.PG_CONN_NAME_BROKER
//Export
export { PgConfig, ConnectionName }