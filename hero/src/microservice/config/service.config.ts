/**
 * Service Config
 */
const MicroserviceConfig = {
    appKey: process.env.APP_KEY,
    name: process.env.HERO_NAME,
    pm: process.env.PM_NAME,
    apiPort: +process.env.HERO_PORT,
    requestTimeout: +process.env.REQUEST_TIMEOUT,
}
//Export
export { MicroserviceConfig }