import { Column, Entity, PrimaryColumn } from "typeorm"
@Entity({
    name: "processes",
})
export class Process {
    @PrimaryColumn({
        type: "character varying",
        length: 36,
        nullable: false,
    })
    id: string;

    @Column({
        type: "character varying",
        length: 50,
        nullable: false
    })
    name: string;
    @Column({
        type: "character varying",
        length: 10,
        nullable: false
    })
    status: string;

    @Column({
        type: "timestamp without time zone",
        nullable: false
    })
    time_create: Date;
    @Column({
        type: "timestamp without time zone",
        nullable: false
    })
    time_end: Date;
    @Column({
        type: "timestamp without time zone",
        nullable: true
    })
    time_close: Date;

    @Column({
        type: "jsonb",
        nullable: false,
    })
    schedule: any;
    @Column({
        type: "jsonb",
        nullable: true,
    })
    data: any;
    @Column({
        type: "jsonb",
        nullable: true,
    })
    input: any;
    @Column({
        type: "jsonb",
        nullable: true,
    })
    error: any;
    @Column({
        type: "jsonb",
        nullable: true,
    })
    task_logs: any;
    @Column({
        type: "integer",
        nullable: true,
    })
    restart_count: number;

}