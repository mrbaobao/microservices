import { BrokerService } from "../broker/broker.service"
import { PgService } from "../lib/pg/pg.service"
import { RedisService } from "../lib/redis/redis.service"
import { Process } from "./process"

class CoreController {
    //Constructor
    constructor(
        //Broker
        protected readonly broker: BrokerService,
        //Redis
        protected readonly redis: RedisService,
        //Pg
        protected readonly pg: PgService,

    ) { }
    /**
     * Create process
     */
    createProcess = async (name: string) => {
        return await Process.new(name, this.broker, this.redis, this.pg)
    }
    /**
     * Get process
     */
    getProcess = async (id: string) => {
        return await Process.get(id, this.broker, this.redis, this.pg)
    }
    /**
     * Force restart closed process
     */
    restartProcess = async (id: string) => {
        return await Process.restart(id, this.broker, this.redis, this.pg)
    }
}
//Export
export { CoreController }