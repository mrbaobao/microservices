import { Module } from "@nestjs/common";
import { CoreController } from "./core.controller";

@Module({
    imports: [
    ],
    providers: [
        CoreController,
    ],
    exports: [
        CoreController,
    ],
})
export class CoreModule { }