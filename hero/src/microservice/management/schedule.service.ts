import { Injectable } from "@nestjs/common";
import { PgService } from "../lib/pg/pg.service";
import { RedisService } from "../lib/redis/redis.service";
import { Schedule } from "./types/schedule.type";

const schedules: { [name: string]: Schedule } = {
    "mission-impossible-1": {
        //Name
        name: "mission-impossible-1",
        //On error: Error thrown from tasks
        on_error: "exit",
        //Start attempts
        restart_attempts: 1,
        //Tasks
        tasks: [
            /**
             * Sequencial tasks: task-1 -> task-2 -> (task-3 & task-4)
             * 1. If task-1 failed --> Execute task-2
             */
            {
                "hero:move-to-moon": {
                    on_error: "next",
                    restart_attempts: 2,
                    input_schema: {
                        "name": "string_not_empty"
                    }
                },
            },
            {
                "hero:move-to-mars": {
                    require: {
                        "hero:move-to-moon": "failed",
                    },
                    on_error: "next",
                    input_schema: {
                        "name": "string_not_empty"
                    }
                },
            },
            /**
             * Parallel tasks: (task-3, task-4)
             */
            {
                "hero:check-aliens": {
                    require: [
                        {
                            "hero:move-to-moon": "success",
                        },
                        {
                            "hero:move-to-mars": "success",
                        },
                    ],
                    input_schema: {
                        "aliens": "boolean"
                    }
                },
                "hero:check-survival-info": {
                    require: [
                        {
                            "hero:move-to-moon": "success",
                        },
                        {
                            "hero:move-to-mars": "success",
                        },
                    ],
                    input_schema: {
                        "survival": "number"
                    }
                },
            },
        ]
    }
}
@Injectable()
class ScheduleService {
    private readonly redis = new RedisService()
    private readonly pg = new PgService()
    /**
     * Get a schedule by name
     * @param name 
     * @returns 
     */
    get = async (name: string): Promise<Schedule | null> => {
        return schedules[name] ?? null
    }
}
//Export
export { ScheduleService }