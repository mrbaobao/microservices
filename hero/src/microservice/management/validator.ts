/**
 * Check str is email
 * @param str 
 * @returns 
 */
const isEmail = (str: string): boolean => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(str.toLowerCase());
}
/**
 * Validator
 */
const Validator = {
    /**
   * Check email
   */
    email: (str: string): boolean => {
        //OK
        return isEmail(str)
    },
    email_optional: (str: any): boolean => {
        str = str ?? ""
        if (!str) {
            return true
        }
        if (Validator.string(str)) {
            //OK
            return isEmail(str)
        }
        return false
    },
    /**
   * Check string
   */
    string: (str: any): boolean => {
        //OK
        return typeof str === "string";
    },
    string_not_empty: (str: any): boolean => {
        //Check empty
        if (!str) {
            return false
        }
        //OK
        return typeof str === "string";
    },
    /**
   * string | undefined
   * @param str 
   */
    string_optional: (str: any): boolean => {
        //Undefined
        if (str === undefined) {
            return true
        }
        //String
        return typeof str === "string";
    },
    /**
   * Optional integer
   * undefined | 123 | "123"
   * @param n 
   */
    integer_optional: (n: any, isPositive: boolean = true): boolean => {
        n = n ?? 0
        //Check empty
        if (n) {
            if (typeof n === "number") {
                if (Number.isInteger(n)) {
                    return isPositive ? n >= 0 : true
                }
            }
            if (typeof n === "string") {
                const num = +n
                if (num + "" === n) {
                    if (Number.isInteger(num)) {
                        return isPositive ? num >= 0 : true
                    }
                }
            }
            //False
            return false
        }
        //OK
        return true;
    },
    "interger+": (id: any): boolean => {
        id = id ?? 0
        //Check empty
        if (id) {
            //False
            return Validator["interger+_optional"](id)
        }
        //OK
        return false
    },
    index: (i: any): boolean => {
        if (i === 0 || i === "0") {
            return true
        }
        return Validator["interger+"](i)
    },
    "interger+_optional": (id: any): boolean => {
        id = id ?? 0
        //Check empty
        if (id) {
            if (typeof id === "number") {
                if (Number.isInteger(id)) {
                    return id > 0
                }
            }
            if (typeof id === "string") {
                const num = +id
                if (num + "" === id) {
                    if (Number.isInteger(num)) {
                        return num > 0
                    }
                }
            }
            //False
            return false
        }
        //OK
        return true;
    },
    positive: (p: any): boolean => {
        if (!p) {
            return false
        }
        //Check empty
        if (typeof p === "number") {
            return p > 0
        }
        if (typeof p === "string") {
            const num = +p
            if (num + "" === p) {
                return num > 0
            }
        }
        //False
        return false
    },
    boolean_optional: (bool: any): boolean => {
        //Check empty
        if (bool !== undefined) {
            return bool === "true" || bool === "false"
        }
        //OK
        return true;
    },
}
/**
 * Types
 */
type ValidateType =
    "string" | "string_optional" | "string_not_empty"
    | "boolean"
    | "number" | "number+"
    | "integer" | "integer+" | "index"
    | "integer_optional" | "integer+_optional" | "index_optional"
    | "email"
    | string[] | number[]
type DataSchema = { [key: string]: ValidateType }
/**
 * Validate
 * @param input 
 * @param inputSchema 
 * @returns 
 */
const Validate = (input: any, inputSchema: DataSchema) => {
    //No scheme required
    if (!inputSchema) {
        return true
    }
    //Check scheme
    const keys = Object.keys(inputSchema)
    if (keys.length > 0 && !input) {
        return false
    }
    let type: ValidateType
    let validator: (data: any) => boolean
    let value: any
    const count = keys.length
    for (let index = 0; index < count; index++) {
        const field = keys[index];
        type = inputSchema[field]
        value = input[field]
        //String
        if (typeof type === "string") {
            validator = Validator[type]
            if (validator) {
                if (!validator(value)) {
                    return false
                }
            }
            //Stop
            return true
        }
        //String array
        return (type as any[]).indexOf(value) >= 0
    }
    //OK
    return true
}
//Export
export type { ValidateType, DataSchema }
export { Validate }
