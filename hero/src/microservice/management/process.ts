import { Logger } from "@nestjs/common"
import { BrokerService } from "../broker/broker.service"
import * as uuid from "uuid"
import { PgService } from "../lib/pg/pg.service"
import { RedisService } from "../lib/redis/redis.service"
import { ScheduleService } from "./schedule.service"
import { Requirements, Task } from "./types/task.type"
import { MicroserviceConfig } from "../config/service.config"
import { Schedule } from "./types/schedule.type"
import { Validate, ValidateType } from "./validator"
import { RedisClientType } from "redis4"

/**
 * Status
 */
enum ProcessStatus {
    new = "new",
    restart = "restart",
    running = "running",
    success = "success",
    failed = "failed",
}
//Cache prefix
const CacheGroup = `${MicroserviceConfig.name}:processes`
/**
 * Messages
 */
const Messages = {
    validateFailed: "Validate input failed",
    requirementsFailed: "Requirements not matched",
}
/**
 * Reason
 */
enum TaskFailedReasons {
    Validate = "validate-failed",
    Error = "error",
}
type TaskLogState = {
    status: ProcessStatus,
    error?: any,
    data?: any,
    time?: Date,
    restart_count?: number,
}
type TaskState = {
    status: ProcessStatus,
    error?: any,
    data?: any,
    time?: Date,
    restart_count?: number,
    logs?: TaskLogState[]
}
/**
 * Core process
 */
class CoreProcess {
    //ID
    id: string = uuid.v4()
    //Is done
    status = ProcessStatus.new
    /**
     * Time
     */
    time_create = new Date()
    time_end: Date
    time_close: Date
    /**
    * Tasks
    */
    task_logs: { [task_name: string]: TaskState } = {}
    //Running tasks
    running_tasks: {
        [task_name: string]: Task
    } = {}
    //Remaining tasks
    remaining_tasks: { [task_name: string]: Task }[] = []

    /**
     * Schedule
     */
    schedule: Schedule
    //Restart count
    restart_count = 0
    /**
     * Is process ended
     */
    is_ended = false
    //Process error
    error: any = null
    //Data
    data: { [key: string]: any }
    //Input: used for restart process
    input: { [key: string]: any }

    //Logger
    protected readonly logger = new Logger("Process")
    //Schedule service
    protected readonly scheduleService = new ScheduleService()
    /**
     * Constructor
     */
    constructor(
        public readonly name: string,
        //Broker
        protected readonly broker: BrokerService,
        //Redis
        protected readonly redis: RedisService,
        //Pg
        protected readonly pg: PgService,
    ) { }
    /**
     * Get cache key with process id
     */
    protected getCacheKey = () => {
        return this.redis.getCacheKey(CacheGroup, `${this.id}`)
    }
    protected getRunningTasksCacheKey = () => {
        return this.redis.getCacheKey(CacheGroup, `${this.id}`) + ":running_tasks"
    }
    /**
     * Store all pid in a set
     * @returns 
     */
    protected getProcessesCacheKey = () => {
        return this.redis.getCacheKey(CacheGroup, ":pids")
    }
    protected static getCacheKey = (id: string, redis: RedisService) => {
        return redis.getCacheKey(CacheGroup, `${id}`)
    }

    /**
     * Update task state
     * # REDIS Hash
     */
    protected cacheAddTaskLog = async (task_name: string, log: TaskLogState,
        redisClient?: RedisClientType) => {
        const cacheKey = this.getRunningTasksCacheKey()
        //Set state
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            //Add time
            const time = new Date()
            log.time = time
            let task_log: TaskState
            //Get from cache
            if (await redisClient.hExists(cacheKey, task_name)) {
                task_log = JSON.parse(await redisClient.hGet(cacheKey, task_name)) as TaskState
                task_log.logs.push(log)
                task_log = {
                    ...log,
                    restart_count: log.restart_count ?? task_log.restart_count,
                    logs: task_log.logs,
                }
            } else {
                task_log = {
                    ...log,
                    logs: [log]
                }
            }
            //Set new cache
            return await redisClient.hSet(cacheKey, task_name, JSON.stringify(task_log))
        })
    }
    /**
     * Get running task state
     */
    protected getRunningTaskState = async (task_name: string, redisClient?: RedisClientType): Promise<TaskState> => {
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            const cacheKey = this.getRunningTasksCacheKey()
            //Get from cache
            if (await redisClient.hExists(cacheKey, task_name)) {
                return JSON.parse(await redisClient.hGet(cacheKey, task_name)) as TaskState
            }
            //Null
            return null
        })
    }
    /**
     * Add log
     * @param log 
     */
    protected addLog = (task_name: string, log: TaskLogState) => {
        console.log(666666666, task_name, log)
        const time = new Date()
        if (!log.time) {
            log.time = time
        }
        //Current task log
        const task_log = this.task_logs[task_name]
        if (task_log) {
            task_log.logs.push(log)
            this.task_logs[task_name] = {
                ...log,
                logs: task_log.logs,
            }
            //Stop
            return
        }
        //New
        this.task_logs[task_name] = {
            ...log,
            logs: [log]
        }
    }
    protected addLogs = (task_name: string, log: TaskState) => {
        console.log(9.99999999, task_name, log)
        //Current task log
        const task_log = this.task_logs[task_name]
        if (task_log) {
            this.task_logs[task_name] = {
                ...log,
                logs: [...task_log.logs, ...log.logs],
            }
            //Stop
            return
        }
        //New
        this.task_logs[task_name] = log
    }
    /**
     * Check running tasks ended
     */
    protected checkRunningTasksEnded = async (redisClient?: RedisClientType) => {
        const keys = Object.keys(this.running_tasks)
        const count = keys.length
        //List is empty
        if (!count) {
            return true
        }
        let isExisted = false
        const cacheKey = this.getRunningTasksCacheKey()
        let task_name = ""
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            /**
             * Check data existed
             */
            for (let i = 0; i < count; i++) {
                isExisted = await redisClient.hExists(cacheKey, keys[i])
                if (!isExisted) {
                    //Stop
                    return false
                }
            }
            /**
             * Update process logs
             */
            for (let i = 0; i < count; i++) {
                task_name = keys[i]
                const task = await this.getRunningTaskState(task_name, redisClient)
                //Logs
                this.addLogs(task_name, task)
                //Merge data
                this.data = { ...this.data, ...task.data }
            }
            //OK
            return true
        })

    }
    /**
     * Add task
     * Note: DO NOT Run task!
     * @param task_name 
     */
    protected addTask = (task_name: string, task: Task) => {
        //Add to running tasks
        this.running_tasks[task_name] = task
        //Add to tasks log
        this.addLog(task_name, {
            status: ProcessStatus.new,
        })
        console.log(4, task_name)
    }
    /**
     * Run all running_tasks
     */
    protected runAll = () => {
        const keys = Object.keys(this.running_tasks)
        const count = keys.length
        for (let i = 0; i < count; i++) {
            //Call and not wait
            this.broker.call(keys[i], { input: this.data, pid: this.id })
        }
        //Update status
        this.status = ProcessStatus.running
        //Return count
        return count
    }
    /**
     * Restart task
     * @param task_name 
     * @returns 
     */
    protected restartTask = async ({ task_name, task, error, status }, redisClient: RedisClientType) => {
        //Check needs restart
        if (!task.restart_attempts) {
            return false
        }
        //Check restart_count
        const cacheKey = this.getRunningTasksCacheKey()
        let restart_count = 0
        let taskLog: TaskState
        if (await redisClient.hExists(cacheKey, task_name)) {
            taskLog = await this.getRunningTaskState(task_name, redisClient)
            restart_count = +(taskLog.restart_count ?? 0)
        }
        console.log("#Process: Restart task", { task_name, restart_count, task })
        if (restart_count < task.restart_attempts) {
            restart_count += 1
            this.broker.call(task_name, { input: this.data, pid: this.id })
            //Time
            const time = new Date()
            //Add restart task
            await this.cacheAddTaskLog(
                task_name,
                {
                    restart_count,
                    status: ProcessStatus.restart,
                    time,
                }
            )
            //Restarted
            return true
        }
        //Not restart
        return false
    }
    /**
     * Update cache
     */
    protected cacheUpdate = async (redisClient?: RedisClientType) => {
        await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            //Pids
            await redisClient.sAdd(this.getProcessesCacheKey(), this.id)
            //Set process info
            return await redisClient.set(this.getCacheKey(), this.toString())
        })
    }
    protected cacheStorePid = async (redisClient?: RedisClientType) => {
        await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            //Pids
            await redisClient.sAdd(this.getProcessesCacheKey(), this.id)
        })
    }
    /**
     * Remove redis cache
     */
    protected cacheRemove = async (redisClient?: RedisClientType) => {
        return await this.redis.start(redisClient, async (redisClient) => {
            //Remove
            await redisClient.del(this.getCacheKey())
            //Remove from pid list
            await redisClient.sRem(this.getProcessesCacheKey(), this.id)
            //Remove hashes
            await redisClient.del(this.getRunningTasksCacheKey())
        })
    }

    /**
     * To json
     */
    protected toString = () => {
        return JSON.stringify(this.toJson())
    }
    protected toJson = () => {
        return {
            id: this.id,
            name: this.name,
            time_create: this.time_create,
            time_end: this.time_end,
            time_close: this.time_close,
            schedule: this.schedule,
            restart_count: this.restart_count,
            status: this.status,
            task_logs: this.task_logs,
            running_tasks: this.running_tasks,
            remaining_tasks: this.remaining_tasks,
            error: this.error,
            data: this.data,
            input: this.input,
            is_ended: this.is_ended,
        }
    }
    /**
     * From json
     */
    protected static fromJson = ({
        name, id,
        schedule,
        input,
        task_logs, running_tasks, remaining_tasks,
        data, error,
        time_create, time_end, time_close,
        status, restart_count,
        is_ended
    },
        broker: BrokerService, redis: RedisService, pg: PgService) => {
        const $p = new Process(name, broker, redis, pg)
        //Update id
        $p.id = id
        //Tasks
        $p.task_logs = task_logs
        $p.running_tasks = running_tasks
        $p.remaining_tasks = remaining_tasks
        //Error
        $p.error = error
        //Data
        $p.data = data
        $p.input = input
        //Schedule
        $p.schedule = schedule
        $p.restart_count = restart_count
        //Time
        $p.time_create = time_create
        $p.time_end = time_end
        $p.time_close = time_close
        //Status
        $p.status = status
        $p.is_ended = is_ended
        //Return process
        return $p
    }
    /**
     * Save process to db
     */
    protected saveToDB = async () => {
        /**
         * Insert to database
         */
        const sql = `
            INSERT INTO processes
                (
                    id, name, time_create, schedule, status,
                    task_logs, time_end, time_close, data, input, error,
                    restart_count
                )
            VALUES
                ($1, $2, $3, $4, $5,
                    $6, $7, $8, $9, $10, $11, $12)
            ON CONFLICT (id)
            DO UPDATE SET 
                status = $5,
                task_logs = $6,
                time_close = $8,
                data = $9,
                error = $11,
                restart_count = $12
        `
        const params = [
            this.id,
            this.name,
            this.time_create,
            this.schedule,
            this.status,

            this.task_logs,//$6
            this.time_end,
            this.time_close,
            this.data,
            this.input,
            this.error,
            this.restart_count,
        ]
        console.log("Insert db", this.toJson(), JSON.stringify(this.task_logs))
        //Query
        return await this.pg.query(sql, params)
    }
    /**
     * Get process from db
     */
    static getProcessFromDB = async (pid: string, pg: PgService) => {
        /**
         * Get from database
         */
        const sql = `
            SELECT *
            FROM processes
            WHERE id = $1
        `
        const params = [
            pid
        ]
        //Query
        const res = await pg.query(sql, params)
        //Return data
        return res.rows[0] ?? null
    }


    /**
     * Get schedule
     */
    protected getSchedule = async () => {
        return await this.scheduleService.get(this.name)
    }
    /**
     * Execute process with data
     */
    protected _start = async (redisClient?: RedisClientType) => {
        console.log("start process ", this.input)
        //Set data
        this.data = this.input
        //Remaining tasks
        this.remaining_tasks = [...this.schedule.tasks]
        //Exec
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            return await this.execRemainingTasks(redisClient)
        })
    }
    /**
     * Stop process
     * 1. Do not exec remaining tasks
     * 2. Waiting for all running tasks completed
     * --> Next: close
     * 3. Save to database
     */
    stop = async ({ error, redisClient }: { error?: any, redisClient?: RedisClientType }) => {
        //Check process ended
        if (this.is_ended) {
            return
        }
        //Update error
        if (error) {
            this.error = error
        }

        //Status
        this.status = this.error ? ProcessStatus.failed : ProcessStatus.success
        /**
         * Ended
         */
        this.is_ended = true
        //Time end
        this.time_end = new Date()
        console.log(99, this.toJson())
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            if (await this.checkRunningTasksEnded(redisClient)) {
                await this.close(redisClient)
                return
            }
            //Update cache
            await this.cacheUpdate(redisClient)
            //Save to database
            await this.saveToDB()
        })
    }
    /**
     * Restart process
     */
    restart = async () => {
        console.log(10000000, "time to restart", new Date())
        //Reset status
        this.status = ProcessStatus.restart
        //Restart count
        this.restart_count += 1
        //Time
        this.time_close = null
        this.time_end = null
        //Error
        this.error = null
        //Is ended state
        this.is_ended = false
        return await this.redis.start(async (redisClient) => {
            //Remove hashes
            await redisClient.del(this.getRunningTasksCacheKey())
            //Start
            return await this._start(redisClient)
        })
    }
    /**
     * Close process after all running tasks completed
     */
    close = async (redisClient?: RedisClientType) => {
        console.log(88, "close process", this.toJson())
        //Only close after process ended
        if (!this.is_ended) {
            return
        }
        /**
         * Check auto restart
         */
        const { restart_attempts } = this.schedule
        if (this.status === ProcessStatus.failed) {
            if (this.restart_count < restart_attempts) {
                //Restart
                await this.restart()
                //Stop
                return
            }
        }
        /**
         * 1. Save to db
         * 2. Remove cache
         */
        return await this.redis.start(redisClient, async (redisClient: RedisClientType) => {
            //Time close
            this.time_close = new Date()
            //Save to database
            await this.saveToDB()
            /**
             * Remove Redis cache
             */
            await this.cacheRemove(redisClient)
        })
    }
    /**
     * Validate input
     * @param input 
     * @param input_schema 
     */
    protected validate = (task_name: string, input: any, input_schema: { [field: string]: ValidateType }) => {
        const ok = Validate(input, input_schema)
        console.log(12, input_schema, input, ok)
        if (!ok) {
            return {
                message: Messages.validateFailed,
                task_name,
                input,
                input_schema,
            }
        }
        //Return
        return null
    }
    /**
     * Check AllRequirements
     * + Array of Requirements: OR
     * + Each Requirements: AND
     * @param task 
     */
    protected checkRequirements = (task_name: string, task: Task) => {
        console.log(10, task)
        //No requirements
        if (!task.require) {
            return null
        }
        /**
         * Convert to Requirements[]
         */
        let allRequirements: Requirements[]
        if (task.require.length === undefined) {
            allRequirements = [task.require as Requirements]
        } else {
            allRequirements = task.require as Requirements[]
        }

        //Check empty
        if (!allRequirements.length) {
            return null
        }
        /**
         * Validate
         */
        const count = allRequirements.length
        for (let i = 0; i < count; i++) {
            const requirements = allRequirements[i];
            const task_names = Object.keys(requirements)
            const tasksCount = task_names.length
            //Check empty conditions
            if (!tasksCount) {
                return null
            }
            let yes = true
            for (let j = 0; j < tasksCount; j++) {
                let task_name = task_names[j]
                if (requirements[task_name] !== this.task_logs[task_name].status) {
                    yes = false
                    break
                }
            }
            //Check requirements
            if (yes) {
                return null
            }
        }
        //Set error
        return {
            message: Messages.requirementsFailed,
            task_name,
            require: task.require,
            task_logs: this.task_logs,
        }
    }
    /**
     * Handle task failed
     */
    protected handleTaskFailed = (task_name: string, task: Task, error?: any): true | false => {

        /**
         * Task log
         */
        console.log(909090, error)
        /**
         * Check task on_error
         */
        if (task.on_error === "next") {
            return true
        }
        /**
         * Check process on_error
         */
        //Schedule
        const { on_error } = this.schedule
        if (task.on_error === "exit" || on_error === "exit"
            || !on_error) {
            if (error) {
                //Add error
                this.error = error
            }
            return false
        }
        //Next
        return true
    }
    /**
     * Exec remaining tasks
     * @returns 
     */
    protected execRemainingTasks = async (redisClient: RedisClientType) => {
        //Reset running task
        this.running_tasks = {}
        /**
         * Start first tasks in parallel
         */
        try {
            //--> Modify this.schedule!
            const parallelTasks: { [task_name: string]: Task } = this.remaining_tasks.shift()
            let task: Task
            /**
             * Start execute tasks
             */
            if (parallelTasks) {
                const task_names = Object.keys(parallelTasks)
                console.log(7, parallelTasks, this.data)
                const count = task_names.length
                for (let i = 0; i < count; i++) {
                    const task_name = task_names[i]
                    task = parallelTasks[task_name]
                    console.log(11, task_name,)
                    /**
                     * Validate input & Check requirements
                     */
                    let error: any = this.validate(task_name, this.data, task.input_schema)
                    if (!error) {
                        error = this.checkRequirements(task_name, task)
                    }
                    if (error) {
                        /**
                         * Validate failed
                         */
                        this.addLog(
                            task_name,
                            {
                                status: ProcessStatus.failed,
                                error,
                            })
                        const next = this.handleTaskFailed(task_name, task, error)
                        console.log(9, task_name)
                        if (next) {
                            continue
                        }
                        //End process
                        await this.stop({
                            redisClient
                        })
                        //Stop
                        return {
                            status: this.status,
                            pid: this.id,
                            error: this.error,
                        }
                    }
                    /**
                     * Validate & check requirements ok
                     */
                    //add task
                    this.addTask(task_name, task)
                }
                //Update cache
                await this.cacheUpdate(redisClient)
                //Run all running_tasks
                const runningTaskCount = this.runAll()
                console.log(8, runningTaskCount, this.remaining_tasks)
                if (!runningTaskCount) {
                    //Execute next
                    return await this.execRemainingTasks(redisClient)
                }
                //Return
                return {
                    status: this.status,
                    pid: this.id,
                }
            } else {
                //Done: All tasks ended
                await this.stop({ redisClient })
                console.log(998756223178544, this.toString())
                //Return
                return {
                    status: this.status,
                    pid: this.id,
                }
            }

        } catch (schedule_error) {
            console.log("schedule_error=", schedule_error)
            //End process
            await this.stop({ error: schedule_error, redisClient })
            //Return
            return {
                status: this.status,
                pid: this.id,
                error: this.error,
            }
        }
    }
}
class Process extends CoreProcess {
    /**
     * New
     */
    static new = async (name: string,
        broker: BrokerService,
        redis: RedisService,
        pg: PgService)
        : Promise<Process> => {
        //New process
        const $p = new Process(name, broker, redis, pg)
        //Get schedule
        $p.schedule = await $p.getSchedule()
        /**
         * Redis
         */
        await $p.redis.start(async (redisClient: RedisClientType) => {
            //Store pid list
            $p.cacheStorePid()
            //Update cache
            await $p.cacheUpdate(redisClient)
        })

        //Return process
        return $p
    }
    /**
     * Get process from id
     * --> Use Redis Pg
     * --> Force restart
     */
    static restart = async (pid: string,
        broker: BrokerService,
        redis: RedisService,
        pg: PgService) => {
        /**
         * Redis
         * Get process from redis
         */
        const cacheKey = this.getCacheKey(pid, redis)
        const isProcessExisted = await redis.start<boolean>(async (redisClient) => {
            return await redisClient.exists(cacheKey) > 0
        })
        //Not found
        if (isProcessExisted) {
            return {
                status: "failed",
                error: {
                    message: "Cannot restart process. The process is running.",
                    pid
                }
            }
        }
        /**
         * Get data from db
         */
        const json = await this.getProcessFromDB(pid, pg)
        if (!json) {
            return {
                status: "failed",
                error: {
                    message: "Process not found",
                    pid
                }
            }
        }
        //Init process
        const $p = this.fromJson(json, broker, redis, pg)

        console.log(1119900, json)
        if ($p.status === ProcessStatus.success) {
            return {
                status: "failed",
                error: {
                    message: "Cannot restart process. The process is successul.",
                    pid
                }
            }
        }
        return $p.restart()
    }
    /**
     * Get process from id
     * --> Use Redis cache
     */
    static get = async (id: string,
        broker: BrokerService,
        redis: RedisService,
        pg: PgService) => {
        /**
         * Redis
         * Get process from redis
         */
        const cacheKey = this.getCacheKey(id, redis)
        const strProcess = await redis.start<string>(async (redisClient) => {
            return await redisClient.get(cacheKey)
        })
        //Not found
        if (!strProcess) {
            return null
        }
        //OK
        const json = JSON.parse(strProcess)
        return this.fromJson(json, broker, redis, pg)
    }
    /**
     * Execute process with data
     */
    start = async (input: { [key: string]: any }) => {
        console.log(20000000, "Process started with input =", input)
        //Set input: READ ONLY!
        this.input = { ...input }
        //Start
        return await this._start()
    }

    /**
     * Execute next tasks of process with input
     * 1. Update running tasks
     * 2. Check all running tasks done
     * 3. Execute next parallel tasks
     */
    next = async ({ data, task_name, status, error }:
        {
            data: any, task_name: string, status: ProcessStatus,
            error: any,
        }) => {
        console.log("next 789", task_name, error)
        //Get task info
        const task: Task = this.running_tasks[task_name]
        if (!task) {
            this.logger.error(`Task not found: ${task_name}, process: ${this.name}:${this.id}`)
            //Stop
            return;
        }
        return await this.redis.start(async (redisClient: RedisClientType) => {
            //Update task state
            await this.cacheAddTaskLog(task_name, {
                status,
                error, data
            }, redisClient)

            //Check process is ended
            if (this.is_ended) {
                console.log("process ended =", task_name)
                //Check running tasks
                if (await this.checkRunningTasksEnded(redisClient)) {
                    //Close
                    await this.close(redisClient)
                }
            }
            /**
             * Step 1: Check status of task
             */
            if (status === ProcessStatus.failed) {
                /**
                 * Process is running: Handle task failed
                 */
                //Restart task
                if (await this.restartTask({ task_name, task, error, status }, redisClient)) {
                    console.log(555555, task_name, this.toJson())
                    //Stop
                    return
                }
                console.log(7777777777, task_name, this.toJson())
                const next = this.handleTaskFailed(task_name, task, error)
                if (!next) {
                    //End
                    await this.stop({ error, redisClient })
                    //Stop
                    return
                }
            }
            /**
             * Step 2: Update task state
             */
            /**
             * Step 3: Check running_tasks state
             */
            const yes = await this.checkRunningTasksEnded(redisClient)
            if (yes) {
                //Exec
                return await this.execRemainingTasks(redisClient)
            }
        })
    }

}
//Export
export { Process }