import { Task } from "./task.type"

interface Schedule {
    //Name
    name: string
    //On error: Error thrown from tasks
    //default: exit
    on_error: "exit" | "next"
    //Restart attempts
    restart_attempts?: number
    //Tasks
    tasks: { [task_name: string]: Task }[]
}
//Export
export { Schedule }