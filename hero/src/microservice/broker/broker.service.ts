import { Injectable } from "@nestjs/common";
import { Transport, ClientProxyFactory, ClientProxy, ClientOptions } from "@nestjs/microservices";
import { createClient, RedisClientType } from "redis4";
import * as uuid from "uuid"
import { RedisConfig } from "../config/redis.config";
import { MicroserviceConfig } from "../config/service.config";
import { RedisOptions } from "../lib/redis/redis.options";
/**
 * Config
 */
const BrokerConfig: ClientOptions = {
    options: {
        url: `redis://${RedisConfig.host}:${RedisConfig.port}`
    },
    transport: Transport.REDIS,
}
//Request timeout
const RequestTimeout = MicroserviceConfig.requestTimeout
/**
 * Microservice name
 */
const Service = MicroserviceConfig.name
const PM = MicroserviceConfig.pm
/**
 * Broker service
 */
class PrivateBrokerService {
    //Broker
    protected readonly broker: ClientProxy
    //Redis pub
    protected readonly redisSub: RedisClientType
    /**
     * Constructor
     */
    constructor() {
        //Broker
        this.broker = ClientProxyFactory.create(BrokerConfig)
        //Redis sub
        this.redisSub = createClient(RedisOptions)
    }

    /**
     * Get register event
     * event = [pm]:[event] | [event]
     * @return register:[from]:[to]:[Event]
     */
    protected getRegisterEvent = (event: string) => {
        let [pm, ev] = event.split(":")
        if (!ev) {
            ev = pm
            pm = PM
        }
        return `register:${Service}:${pm}:${ev}`
    }
    /**
     * Get respond event
     * @return register_respond:[mid]
     */
    protected getRegisterRespondEvent = (mid: string) => {
        return `register_respond:${mid}`
    }

    /**
     * Get call event
     * @return task:[pm]:[hero]:[Event]
     */
    protected getCallEvent = (task_name: string) => {
        return `task:${Service}:${task_name}`
    }
    /**
     * Get Task respond event
     * @return task_respond:[pm]:[hero]:[Event]
     */
    protected getTaskRespondEvent = (from: string, event: string) => {
        return `task_respond:${from}:${Service}:${event}`
    }
    /**
     * Get request event
     * @return request:[pm]:[hero]:[Event]
     */
    protected getRequestEvent = (service: string, event: string) => {
        return `request:${Service}:${service}:${event}`
    }
    /**
     * Get Respond event
     * @return respond:[mid]
     */
    protected getRespondEvent = (mid: string) => {
        return `respond:${mid}`
    }
    /**
     * Get publish event
     */
    protected getPublishEvent = (event: string) => {
        return `${Service}:${event}`
    }
    /**
     * Request with respond
     */
    protected _request = <TResult = any, TInput = any>(
        event: string,
        respondEvent: string,
        data: TInput,
    ) => {
        return new Promise<any>(async (res, rej) => {
            //Check open
            if (!this.redisSub.isOpen) {
                await this.redisSub.connect()
            }
            //Subscribe event
            await this.redisSub.subscribe(respondEvent,
                (str: string) => {
                    //Unsubscribe event
                    this.redisSub.unsubscribe(respondEvent)
                    //Respond with data
                    const { data } = JSON.parse(str)
                    res(data)
                })
            //Check timeout
            setTimeout(() => {
                //Unsubscribe event
                this.redisSub.unsubscribe(respondEvent)
                //Reject
                rej(`Call event ${event} error: Timeout`)
            }, RequestTimeout)
            //Emit
            this.broker.emit<TResult, TInput>(event, data)
        })
    }
}
@Injectable()
class BrokerService extends PrivateBrokerService {
    /**
     * Register event
     * event --> [DestinationPM]:[EventName]
     */
    register = <TResult = any, TInput = any>(event: string, data: TInput) => {
        const mid = uuid.v4()
        return this._request<TResult, TInput>(
            this.getRegisterEvent(event),
            this.getRegisterRespondEvent(mid),
            {
                ...data,
                mid
            },
        )
    }
    /**
     * Call service
     * @param event 
     * @param data 
     */
    call = <TResult = any, TInput = any>(task_name: string, data: TInput) => {
        const mid = uuid.v4()
        //Emit
        this.broker.emit<TResult, TInput>(
            this.getCallEvent(task_name),
            {
                ...data,
                mid
            })
    }
    /**
     * Request
     * @param event 
     * @param data 
     */
    request = <TResult = any, TInput = any>(service: string, event: string, data: TInput) => {
        const mid = uuid.v4()
        //Emit
        return this._request<TResult, TInput>(
            this.getRequestEvent(service, event),
            this.getRespondEvent(mid),
            {
                ...data,
                mid
            })
    }
    /**
     * Publish
     */
    publish = <TResult = any, TInput = any>(event: string, data: TInput) => {
        return this.broker.emit<TResult, TInput>(this.getPublishEvent(event), data)
    }
    /**
     * Respond
     */
    respond = <TResult = any, TInput = any>(mid: string, data: TInput) => {
        return this.broker.emit<TResult, TInput>(
            this.getRespondEvent(mid),
            data,
        )
    }
    /**
     * Register Respond
     */
    registerRespond = <TResult = any, TInput = any>(mid: string, data: TInput) => {
        return this.broker.emit<TResult, TInput>(
            this.getRegisterRespondEvent(mid),
            data,
        )
    }
    /**
     * TaskRespond
     */
    taskRespond = <TResult = any, TInput = any>(from: string, event: string, data: TInput) => {
        return this.broker.emit<TResult, TInput>(this.getTaskRespondEvent(from, event), data)
    }
}
//Export
export { BrokerService }