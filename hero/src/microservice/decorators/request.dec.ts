import { DECOR_KEYS_REQUEST } from "./constants"

/**
 * Method decorator
 * Add respond
 * Note:
 *  <target> in MethodDecorator = <target.prototype> in ClassDecorator
 */
function RequestPattern(pattern: any) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Define metadata
        Reflect.defineMetadata(`${DECOR_KEYS_REQUEST}:${propertyKey}`, pattern, target)
        //Return descriptor
        return descriptor
    }
}
/**
 * Export
 */
export { RequestPattern } 