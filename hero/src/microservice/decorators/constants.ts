
const DECOR_KEYS_REGISTER = "ZAA_DECOR_METHOD_REGISTER"
const DECOR_KEYS_REQUEST = "ZAA_DECOR_METHOD_REQUEST"
const DECOR_KEYS_TASK = "ZAA_DECOR_METHOD_TASK"
const DECOR_KEYS_TASK_RESPOND = "ZAA_DECOR_METHOD_TASK_RESPOND"
//Export
export {
    DECOR_KEYS_REGISTER, DECOR_KEYS_REQUEST,
    DECOR_KEYS_TASK, DECOR_KEYS_TASK_RESPOND,
}