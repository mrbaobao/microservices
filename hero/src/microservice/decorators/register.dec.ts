import { DECOR_KEYS_REGISTER } from "./constants"

/**
 * Method decorator
 * <ServiceName>:register:<EventName>
 * Note:
 *  <target> in MethodDecorator = <target.prototype> in ClassDecorator
 */
function RegisterPattern(event: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Define metadata
        Reflect.defineMetadata(`${DECOR_KEYS_REGISTER}:${propertyKey}`, event, target)
        //Return descriptor
        return descriptor
    }
}
/**
 * Export
 */
export { RegisterPattern } 