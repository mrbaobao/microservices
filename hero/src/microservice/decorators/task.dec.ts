import { DECOR_KEYS_TASK } from "./constants"

/**
 * Method decorator
 * Subscribe Task from ProjectManager
 * Note:
 *  <target> in MethodDecorator = <target.prototype> in ClassDecorator
 */
function TaskPattern(pattern: any) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Define metadata
        Reflect.defineMetadata(`${DECOR_KEYS_TASK}:${propertyKey}`, pattern, target)
        //Return descriptor
        return descriptor
    }
}
/**
 * Export
 */
export { TaskPattern } 