import { INestApplication } from "@nestjs/common"
import { NestFactory } from "@nestjs/core"
import { Transport } from "@nestjs/microservices"
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger"
import { RedisConfig } from "../config/redis.config"
import { MicroserviceConfig } from "../config/service.config"
/**
 * Constants
 */
const Colors = {
    clearColor: "\u001b[1;0m",
    green: "\u001b[1;32m",
    yellow: "\u001b[1;33m",
}
/**
 * Swagger api document
 */
const createDocument = (app: INestApplication, {
    title, tags, version, path
}: { title: string, tags: string[], version: string, path: string }) => {
    const options = new DocumentBuilder()
        .setTitle(title)
    //tags
    tags.forEach((tag) => {
        options.addTag(tag)
    })
    //Version
    options.setVersion(version)
    //Document
    const document = SwaggerModule.createDocument(app, options.build());
    SwaggerModule.setup(path, app, document);
}
/**
 * Bootstrap
 * @param module 
 * @param documentConfig 
 */
const bootstrap = async (
    module: any,
    documentConfig?: { title: string, tags: string[], version: string, path: string }) => {
    //App
    const app = await NestFactory.create(module)
    //Microservice
    app.connectMicroservice({
        transport: Transport.REDIS,
        options: {
            url: `redis://${RedisConfig.host}:${RedisConfig.port}`
        },
    });
    /**
     * Document
     */
    if (documentConfig) {
        createDocument(app, documentConfig)
    }

    //Start all services
    await app.startAllMicroservices()
    //Listen
    await app.listen(MicroserviceConfig.apiPort, () => {
        //Service name
        let serviceName = MicroserviceConfig.name ?? "service"
        serviceName = Colors.yellow + serviceName.toUpperCase() + Colors.clearColor
        //Log
        console.log(`Microservice ${serviceName} listening on port ` + (Colors.green + MicroserviceConfig.apiPort + Colors.clearColor))
    });

}
//Export
export { bootstrap }