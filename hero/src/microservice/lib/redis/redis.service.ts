import { createClient, RedisClientType } from 'redis4';
import * as uuid from 'uuid';
import { CacheServiceOptions } from './redis.types';
import { Injectable, Logger } from '@nestjs/common';
import { RedisOptions } from './redis.options';
const enum Status {
  BUSY,
  FREE,
}

interface Resource {
  redisClient: RedisClientType;
  status: Status;
  id: string;
}

type BorrowCallback<T> = (client: RedisClientType) => Promise<T>

@Injectable()
class RedisService {
  private readonly logger = new Logger("RedisService")
  /**
   * Free resources: id-resource
   * Note: using map to ignore duplicate
   */
  private freeResources = new Map<string, Resource>()
  //In use resources: id-resource
  private busyResources = new Map<string, Resource>()
  //Options
  private readonly options: CacheServiceOptions = RedisOptions
  /**
   * Construct
   */
  constructor() { }
  /**
   * Clear all
   */
  clearAll = async () => {
    return await this.start(async (client) => {
      return await client.flushAll()
    })
  }
  /**
   * Get redis client from pool
   */
  getResource = async () => {
    //Find an idle
    let [id] = this.freeResources.keys()

    let idleResource: Resource
    //Not found resource
    if (id) {
      //Get
      idleResource = this.freeResources.get(id)
      //Remove from free resources
      this.freeResources.delete(id)
    } else {
      //Create new
      idleResource = await this.createResource();
    }
    //Set status BUSY
    idleResource.status = Status.BUSY;
    //Push to bust resources
    this.busyResources.set(idleResource.id, idleResource);
    //Return client
    return idleResource
  }

  /**
   * Release resource
   */
  private releaseResource = async (resource: Resource) => {
    const id = resource.id
    //Change status
    resource.status = Status.FREE;
    //Disconnect
    // await resource.redisClient.disconnect()
    //Add to free resources
    this.freeResources.set(id, resource)
    //Remove from busy
    this.busyResources.delete(id)
  }

  /**
   * Mượn resource xong tự động trả lại sau khi cb hoàn thành hoặc lỗi
   * @param cb function thực thi logic
   */
  start = async <T>(
    redisClient: RedisClientType | BorrowCallback<T>,
    cb?: BorrowCallback<T>) => {
    let resource: Resource
    let needsRelease = true
    if (cb && redisClient) {
      /**
       * Receive redisClient from out side
       * --> No release
       */
      redisClient = redisClient as RedisClientType
      const id = this.getResourceId(redisClient)
      resource = this.busyResources.get(id)
      needsRelease = false
    } else {
      cb = cb ?? redisClient as BorrowCallback<T>
      resource = await this.getResource()
    }

    const client = resource.redisClient
    //Connect client
    if (!client.isOpen) {
      await client.connect()
    }

    try {
      return await cb(client);
    } catch (e) {
      this.logger.error("RedisService.start", e)
    } finally {
      if (needsRelease) {
        await this.releaseResource(resource);
      }

    }
  }
  /**
   * Create resource
   * @returns 
   */
  private createResource = async (): Promise<Resource> => {
    this.logger.log({
      "RedisService.createResource: freeResources": this.freeResources.size
    })
    const clientCount = this.freeResources.size + this.busyResources.size
    if (clientCount >= this.options.maxClients) {
      this.logger.log({ "RedisService.createResource: Reach maxClients": clientCount })
      throw new Error('RedisService.createResource: Reach maxClients');
    }

    const redisClient = createClient(this.options);
    ///Add id
    const id = uuid.v4()
    this.setResourceId(redisClient, id)
    //Resource
    const resource: Resource = {
      redisClient,
      status: Status.FREE,
      id
    }
    redisClient.on('error', async (error) => {
      await this.destroyResource(resource);
      this.handleError(error);
    });
    /**
     * Store for report
     */
    await this.countRedisClient(redisClient)
    //Return resource
    return resource
  }
  /**
   * Count redis clients
   * @param redisClient 
   */
  countRedisClient = async (redisClient: RedisClientType) => {
    await redisClient.connect()
    await redisClient.set(this.getCacheKey("redis-clients", `${process.pid}`), this.freeResources.size + this.busyResources.size)

  }
  /**
   * Destroy resource from pool
   */
  private destroyResource = async (resource: Resource) => {
    const id = resource.id
    //Quit!
    await resource.redisClient.quit();
    //Delete from free resources
    this.freeResources.delete(id)
    //Delete from busy resources
    this.busyResources.delete(id)
    /**
     * Count
     */
    this.start(async (redisClient) => {
      await this.countRedisClient(redisClient)
    })
  }
  /**
   * Error
   * @param error 
   */
  private handleError = (error: any) => {
    this.logger.error("RedisService.handleError", error);
  }
  /**
   * Get resource id from client
   * @param redisClient 
   * @returns 
   */
  private setResourceId = (redisClient: RedisClientType, id: string) => {
    redisClient["resourceId"] = id
  }
  private getResourceId = (redisClient: RedisClientType) => {
    return redisClient["resourceId"]
  }
  /**
   * Get a cache key
   */
  getCacheKey = (group: string, key?: string) => {
    if (key) {
      return group + ":" + key
    }
    //Root 
    return group
  }
}
//Export
export { RedisService }
export type { BorrowCallback }