import { RedisClientOptions } from "redis4";

interface CacheServiceOptions extends RedisClientOptions {
    maxClients: number,
    socket?: any
}
//Export
export type { CacheServiceOptions }