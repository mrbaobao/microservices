import { Injectable, Logger } from "@nestjs/common";
import { PgPool } from "./pg-pool";
import { ConnectionName } from "../../config/pg.config";

@Injectable()
class PgService {
    name: string = "PgProvider"
    protected pg: PgPool
    private readonly logger = new Logger()
    //Constructor
    constructor() {
        this.pg = new PgPool(ConnectionName)
    }

    //Query
    async query(strQuery: string, params: any[] = []) {
        try {
            return this.pg.query(strQuery, params);
        } catch (error) {
            this.logger.error({ "PgSerice query error:": error });
            error['api-name'] = this.name;
            error['query-string'] = strQuery;
            error['params'] = params;
            return Promise.reject(error);
        }
    }
}
//Export
export { PgService }