import { Injectable, Logger } from "@nestjs/common";
import * as pg from "pg"
import { PgConfig } from "../../config/pg.config";

@Injectable()
class PgPool {
    //Logger
    private readonly logger = new Logger("PgPool")
    //PG pool
    private pool: pg.Pool;
    public client: pg.Client = null
    /**
     * Constructor
     */
    constructor(connection: string) {
        this.createPool(connection);
    }
    /**
     * Connect
     */
    public pgConnect() {
        return this.pool.connect();
    }

    /**
     * @memberOf PgConnect
     */
    public async query(sql: string, params: any[] = []): Promise<pg.QueryResult> {
        if (this.client) {
            return this.client.query(sql, params)
        } else {
            const client = await this.pool.connect();
            try {
                const res = await client.query(sql, params);
                client.release();
                return res;
            } catch (error) {
                client.release();
                return await Promise.reject(error);
            }

        }
    }
    /**
     * Create pg pool
     */
    private createPool(connection: string) {
        //Pool
        this.pool = new pg.Pool(PgConfig);
        /**
         * Listen error
         */
        this.pool.on('error', (error: any, client: any) => {
            this.logger.error({ "PgPool:createPool:error:": { client, error } });
        })
    }
    /**
     * Shut down pg
     */
    public shutDown() {
        this.pool.end();
    }
    /**
     * Close a transaction
     */
    public closeConnection() {
        this.client = null
    }
}
//Export
export { PgPool }