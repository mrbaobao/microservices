import { Injectable } from "@nestjs/common";
import axios, { AxiosInstance } from "axios"
import { HttpOptions } from "./http.options";

@Injectable()
class Http {
    //Axios
    axios: AxiosInstance
    /**
     * Constructor
     */
    constructor() {
        this.axios = axios.create();
    }

    /**
     * Post
     */
    post = async (path: string, params?: any) => {
        return await this._post(path, params)
    }
    private _post = async (path: string, params: any) => {
        return await axios.post(path, params, HttpOptions)
    }
    /**
     * Get
     */
    get = async (path: string, params?: any) => {
        return await this._get(path, { params })
    }
    private _get = async (path: string, params: any) => {
        return await axios.get(path, { params, ...HttpOptions })
    }
}
//Export
export { Http }