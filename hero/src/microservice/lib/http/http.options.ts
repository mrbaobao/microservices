import { MicroserviceConfig } from "../../config/service.config";
const HttpOptions = {
    timeout: 1000,
    headers: {
        'app-key': MicroserviceConfig.appKey,
    }
}
//Export
export { HttpOptions }