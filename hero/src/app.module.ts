import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { HeroesModule } from './modules/heroes/heroes.module';
import { UsersModule } from './modules/users/users.module';
import { LoggerMiddleware } from './util/logger.middleware';

@Module({
  imports: [UsersModule, HeroesModule],
  controllers: [],
  providers: [
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('*');
  }
}
