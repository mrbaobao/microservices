import { CommandHandler, EventBus, ICommandHandler } from "@nestjs/cqrs";
import { HeroKilledDragonEvent } from "../events/hero-killed-dragon.event";
import { HeroRepository } from "../repository/hero.repository";

export class KillDragonCommand {
  constructor(
    public readonly heroId: string,
    public readonly dragonId: string,
  ) { }
}

@CommandHandler(KillDragonCommand)
export class KillDragonCommandHandler implements ICommandHandler<KillDragonCommand> {
  constructor(
    private readonly repository: HeroRepository,
    private readonly eventBus: EventBus,
  ) { }

  async execute(command: KillDragonCommand) {
    console.log('KillDragonCommand...');

    const { heroId, dragonId } = command;

    // const hero = this.publisher.mergeObjectContext(
    //   await this.repository.findOneById(heroId),
    // );

    // console.log('da kill')

    // //ban event
    // hero.killEnemy(dragonId);

    // hero.commit();

    this.eventBus.publish(new HeroKilledDragonEvent(heroId, dragonId));
  }
}
