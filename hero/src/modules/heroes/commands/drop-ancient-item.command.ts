import { CommandHandler, EventPublisher, ICommandHandler } from "@nestjs/cqrs";
import { HeroRepository } from "../repository/hero.repository";
import { BrokerService } from '../../../microservice';

export class DropAncientItemCommand {
  constructor(
    public readonly heroId: string,
    public readonly itemId: string
  ) { }

}

@CommandHandler(DropAncientItemCommand)
export class DropAncientIteCommandmHandler
  implements ICommandHandler<DropAncientItemCommand> {
  constructor(
    private readonly broker: BrokerService,
    private readonly repository: HeroRepository,
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: DropAncientItemCommand) {
    console.log('Async DropAncientItemCommand...');

    const { heroId, itemId } = command;
    const hero = this.publisher.mergeObjectContext(
      await this.repository.findOneById(heroId),
    );

    hero.addItem(itemId);
    hero.commit();
  }
}
