import { Module } from '@nestjs/common';
import { HeroesService } from './heroes.service';
import { HeroesController } from './heroes.controller';
import { CqrsModule, EventsHandler } from '@nestjs/cqrs';
import { HeroRepository } from './repository/hero.repository';
import { QueryHandlers } from './queries';
import { CommandHandlers } from './commands';
import { EventHandlers } from './events';
import { HeroesGameSagas } from './heroes.sagas';
import { BrokerModule } from '../../microservice';
import { HeroesMessages } from './heroes.messages';

@Module({
  imports: [
    CqrsModule,
    BrokerModule
  ],
  controllers: [
    HeroesController,
    HeroesMessages,
  ],
  providers: [
    HeroRepository,
    HeroesService,
    ...QueryHandlers,
    ...CommandHandlers,
    ...EventHandlers,
    HeroesGameSagas
  ]
})
export class HeroesModule { }
