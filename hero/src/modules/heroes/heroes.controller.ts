import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HeroesService } from './heroes.service';
import { CreateHeroDto, KillDragonDto } from './dto/create-hero.dto';
import { GetHeroesQuery } from './queries/get-heroes.query';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { KillDragonCommand } from './commands/kill-dragon.command';
import { GetOneHeroesQuery } from './queries/get-one-heroes.query';
import { BrokerService } from '../../microservice';

@Controller('heroes')
export class HeroesController {
  constructor(
    private readonly broker: BrokerService,
    private readonly heroesService: HeroesService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus) { }

  @Post()
  create(@Body() createHeroDto: CreateHeroDto) {
    return this.heroesService.create(createHeroDto);
  }

  @Get()
  findAll() {
    console.log("Step #1. Client request api /heroes to Service [hero].")
    return this.queryBus.execute(new GetHeroesQuery());
  }

  @Post(':id/kill')
  async killDragon(@Param('id') id: string, @Body() dto: KillDragonDto) {
    return this.commandBus.execute(new KillDragonCommand(id, dto.dragonId));
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    console.log('Controller.findOne')
    return this.queryBus.execute(new GetOneHeroesQuery(id));
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateHeroDto: UpdateHeroDto) {
  //   return this.heroesService.update(+id, updateHeroDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.heroesService.remove(+id);
  // }
}
