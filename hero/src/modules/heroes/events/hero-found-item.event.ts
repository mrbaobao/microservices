import { EventsHandler, IEventHandler } from "@nestjs/cqrs";

export class HeroFoundItemEvent {
  constructor(
    public readonly heroId: string,
    public readonly itemId: string
  ) { }
}


@EventsHandler(HeroFoundItemEvent)
export class HeroFoundItemHandler implements IEventHandler<HeroFoundItemEvent> {
  handle(event: HeroFoundItemEvent) {
    console.log('Async HeroFoundItemEvent...');
  }
}
