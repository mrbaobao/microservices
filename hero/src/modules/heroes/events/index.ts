import { HeroFoundItemHandler } from "./hero-found-item.event";
import { HeroKilledDragonHandler } from "./hero-killed-dragon.event";

export const EventHandlers = [HeroKilledDragonHandler, HeroFoundItemHandler];
