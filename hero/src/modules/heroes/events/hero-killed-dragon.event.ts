import { EventsHandler, IEventHandler } from "@nestjs/cqrs";

export class HeroKilledDragonEvent {
  constructor(
    public readonly heroId: string,
    public readonly dragonId: string,
  ) { }
}

@EventsHandler(HeroKilledDragonEvent)
export class HeroKilledDragonHandler implements IEventHandler<HeroKilledDragonEvent> {

  handle(event: HeroKilledDragonEvent) {
    console.log('HeroKilledDragonEvent...');
  }
}