import {
  Logger,
} from '@nestjs/common';
import { Ctx, Payload, RedisContext } from '@nestjs/microservices';
import { BrokerService, Controller, TaskPattern, TaskRespond } from '../../microservice';

@Controller()
export class HeroesMessages {
  //Logger
  private readonly logger = new Logger("HeroesMessagesController")
  constructor(
    private readonly broker: BrokerService
  ) { }

  /**
   * Task 1
   */
  @TaskPattern("pm:move-to-moon")
  public async handler1(
    @Payload() {
      pid, input
    },
  ) {
    console.log("Step #5. Service [hero] receive Task pm:move-to-moon")
    await new Promise((res) => {
      setTimeout(() => {
        res(1)
      }, 2000)
    })
    return TaskRespond.failed({
      message: "I don't like this mission!",
    })
  }
  /**
   * Task 2
   */
  @TaskPattern("pm:move-to-mars")
  public async handler2(
    @Payload() data: {
      pid: string,
      input: any,
    },
  ): Promise<any> {
    console.log("hero:move-to-mars", data)
    return TaskRespond.success({
      has_aliens: false,
      survival: 0.8,
    })
  }
  /*
   * Task 3
   */
  @TaskPattern("pm:check-aliens")
  public async handler3(
    @Payload() data: {
      pid: string,
      input: any,
    }
  ): Promise<any> {
    console.log("pm:check-aliens", data)
    return TaskRespond.success({
      note: "We are still lonely in this universe!"
    })
  }
  /*
   * Task 4
   */
  @TaskPattern("pm:check-survival-info")
  public async handler4(
    @Payload() data: {
      pid: string,
      input: any,
    }
  ): Promise<any> {
    console.log("pm:check-survival-info starting...", data)
    await new Promise((res) => {
      setTimeout(() => {
        res(1)
      }, 5000)
    })
    console.log("pm:check-survival-info done")
    return TaskRespond.success({
      note: "We will move to Mars in 2050!",
    })
  }

}
