import { Injectable } from "@nestjs/common";
import { userHero } from "./fixtures/user";
import { Hero } from "./hero.model";

@Injectable()
export class HeroRepository {
  async findOneById(id: string): Promise<Hero> {
    //xu  ly trong database cai gi do roi tra ve object du lieu
    console.log("HeroRepository.findOneById")
    for (let index = 0; index < userHero.length; index++) {
      if (userHero[index].name == id) {
        return userHero[index];
      }
    }
    return null;
  }

  async findAll(): Promise<Hero[]> {
    return userHero;
  }
}
