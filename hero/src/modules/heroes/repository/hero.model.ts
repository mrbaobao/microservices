import { AggregateRoot } from '@nestjs/cqrs';
import { HeroKilledDragonEvent } from '../events/hero-killed-dragon.event';

export class Hero extends AggregateRoot {
  public name: string;

  constructor(private readonly id: string) {
    super();
    this.name = id;
  }

  killEnemy(enemyId: string) {
    // logic
    this.apply(new HeroKilledDragonEvent(this.id, enemyId));
  }

  addItem(itemId: string) {
    // logic
    // this.apply(new HeroFoundItemEvent(this.id, itemId));
  }
}
