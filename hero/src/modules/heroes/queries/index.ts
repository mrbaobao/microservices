import { GetHeroesQueryHandler } from "./get-heroes.query";
import { GetOneHeroesQueryHandler } from "./get-one-heroes.query";

export const QueryHandlers = [GetOneHeroesQueryHandler,GetHeroesQueryHandler];
