import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { BrokerService } from "../../../microservice";
import { HeroRepository } from "../repository/hero.repository";

export class GetHeroesQuery { }


@QueryHandler(GetHeroesQuery)
export class GetHeroesQueryHandler implements IQueryHandler<GetHeroesQuery> {
    constructor(
        private readonly repository: HeroRepository,
        private readonly broker: BrokerService,
    ) { }

    async execute(query: GetHeroesQuery) {
        const users = await this.repository.findAll();
        const event = "pm:start-mission-impossible"
        //Publish to Redis Message broker
        try {
            console.log("Step #2. Service [hero] Query")
            const { error, pid } = await this.broker.register(event, {
                name: "Tom Cruise",
            })
            if (error) {
                console.log(`Register event ${event} error: `, error)
                return {
                    error
                }
            }
            console.log(`Step #5. Service [hero] REGISTER event ${event} success with pid = ${pid}`)
            return {
                users,
                pid
            }
        } catch (error) {
            console.log(`Register event ${event} error: `, error)
            return {
                error
            }
        }
    }
}
