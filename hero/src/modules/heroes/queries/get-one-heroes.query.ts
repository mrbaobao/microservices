import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { HeroRepository } from "../repository/hero.repository";

export class GetOneHeroesQuery {
    constructor(
        public readonly heroId: string,
    ) { }
}


@QueryHandler(GetOneHeroesQuery)
export class GetOneHeroesQueryHandler implements IQueryHandler<GetOneHeroesQuery> {
    constructor(private readonly repository: HeroRepository) { }

    async execute(query: GetOneHeroesQuery) {
        const { heroId } = query;
        console.log('GetOneHeroesQueryHandler...');
        return this.repository.findOneById(heroId);
    }
}
