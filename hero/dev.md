# Nestjs
+ nestjs@8.0.0
+ redis@3.1.2

# Docker commands

1. Remove all
sudo docker system prune -a --volumes
sudo docker rm -vf $(docker ps -aq)
sudo docker rmi -f $(docker images -aq)
sudo docker volume rm $(docker volume ls -q)

2. Docker compose
sudo docker-compose up
sudo docker-compose down

3. Docker
sudo docker build -t task .
docker run --env-file ../.env --publish 8002:8003 broker

4. 