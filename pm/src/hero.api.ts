import {
  Inject,
  Logger, Controller, Get, Body, Param, Query,
} from '@nestjs/common';
import { ProviderKeys } from './services/constants/providers.keys';
import {
  PgService, RedisService,
  BrokerService,
  CoreController,
} from './microservice';

@Controller("process")
export class HeroApiController extends CoreController {
  //Logger
  private readonly logger = new Logger("TaskController")
  //Constructor
  constructor(
    //Broker
    protected readonly broker: BrokerService,
    //Redis
    @Inject(ProviderKeys.Redis) protected readonly redis: RedisService,
    //Pg
    @Inject(ProviderKeys.Pg) protected readonly pg: PgService,
  ) {
    super(broker, redis, pg)
  }

  /**
   * From service: hero
   * Message: get-heroes-done
   * @returns 
   */

  @Get("restart")
  public async restartProcessHandler(
    @Query() { pid }: { pid: string }
  ): Promise<any> {
    /**
     * Get process --> restart
     */
    //Execute
    return await this.restartProcess(pid)
  }

}
