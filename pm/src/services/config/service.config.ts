class ServiceConfig {
  private readonly envConfig: { [key: string]: any } = null;

  constructor() {
    //Init
    this.envConfig = {};

  }
  /**
   * Get config
   * @param key 
   * @returns 
   */
  get(key: string): any {
    return this.envConfig[key];
  }
}
//Export
export { ServiceConfig }