class ProviderKeys {
    static readonly Http = "HTTP"
    static readonly Redis = "REDIS"
    static readonly Pg = "POSTGRES"
}
//Export
export { ProviderKeys }