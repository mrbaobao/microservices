import {
  Inject,
  Logger,
} from '@nestjs/common';
import { Ctx, Payload, RedisContext } from '@nestjs/microservices';
import { ProviderKeys } from './services/constants/providers.keys';
import {
  PgService, RedisService,
  Controller, RegisterPattern, BrokerService,
  TaskResponsePattern, CoreController,
} from './microservice';

@Controller()
export class HeroController extends CoreController {
  //Logger
  private readonly logger = new Logger("TaskController")
  //Constructor
  constructor(
    //Broker
    protected readonly broker: BrokerService,
    //Redis
    @Inject(ProviderKeys.Redis) protected readonly redis: RedisService,
    //Pg
    @Inject(ProviderKeys.Pg) protected readonly pg: PgService,
  ) {
    super(broker, redis, pg)
  }

  /**
   * From service: hero
   * Message: get-heroes-done
   * @returns 
   */

  @RegisterPattern("hero:start-mission-impossible")
  public async handleGetHeroesDone(
    @Payload() { name }: {
      name: string
    }
  ): Promise<any> {
    console.log("Step #3. PM received REGISTER from Service [hero]")
    /**
     * Start a process
     */
    const processName = "mission-impossible-1"
    const $p = await this.createProcess(processName)
    const pid = $p.id

    console.log("Step #4. PM start a PROCESS done, pid = ", pid)
    //Execute
    return await $p.start({
      name,
      pid
    })
  }
  /**
   * Listen respond from service
   * @param data
   */
  @TaskResponsePattern("hero:move-to-moon")
  public async handleTaskRespond(
    @Payload() {
      pid,//Process id
      status,
      data,
      error,
    },
  ) {
    console.log("Step #5.1. PM receive TaskRespond from Service [hero] move-to-moon", {
      status, error, data,
    })
    /**
     * Get process from pid
     */
    const $p = await this.getProcess(pid)
    //Check process
    if (!$p) {
      return
    }
    await $p.next(
      {
        data,
        status,
        task_name: "hero:move-to-moon",
        error,
      }
    )
  }
  /**
   * Listen respond from service
   * @param data
   */
  @TaskResponsePattern("hero:move-to-mars")
  public async handleTaskRespond2(
    @Payload() {
      pid,//Process id
      status,
      data,
      error,
    },
  ) {
    console.log("#PM. PM receive TaskRespond from Service [hero] move-to-mars",
      {
        status, data, error
      })
    /**
     * Get process from pid
     */
    const $p = await this.getProcess(pid)
    //Check process
    if (!$p) {
      return
    }
    await $p.next(
      {
        data,
        status,
        task_name: "hero:move-to-mars",
        error,
      }
    )
  }
  /**
   * Listen respond from service
   * @param data
   */
  @TaskResponsePattern("hero:check-aliens")
  public async handleTaskRespond3(
    @Payload() {
      pid,//Process id
      status,
      data,
      error,
    },
  ) {
    console.log("#PM. PM receive TaskRespond from Service [hero] check-aliens",
      {
        status, data, error
      })
    /**
     * Get process from pid
     */
    const $p = await this.getProcess(pid)
    //Check process
    if (!$p) {
      return
    }
    await $p.next(
      {
        data,
        status,
        task_name: "hero:check-aliens",
        error,
      }
    )
  }
  /**
   * Listen respond from service
   * @param data
   */
  @TaskResponsePattern("hero:check-survival-info")
  public async handleTaskRespond4(
    @Payload() {
      pid,//Process id
      status,
      data,
      error,
    },
  ) {
    /**
     * Get process from pid
     */
    console.log("#PM. PM receive TaskRespond from Service [hero] check-survival-info",
      {
        status, data, error
      })
    const $p = await this.getProcess(pid)
    //Check process
    if (!$p) {
      return
    }
    await $p.next(
      {
        data,
        status,
        task_name: "hero:check-survival-info",
        error,
      }
    )
  }

}
