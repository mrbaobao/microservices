import { Module } from '@nestjs/common';
import { HeroController } from './hero.controller';
import { ProviderKeys } from './services/constants/providers.keys';
import { BrokerModule, CoreController, Http, PgService, RedisService } from './microservice';
import { HeroApiController } from './hero.api';

@Module({
  imports: [
    CoreController,
    BrokerModule,
  ],
  controllers: [
    HeroController,
    HeroApiController
  ],
  providers: [
    /**
     * Pg
     */
    {
      provide: ProviderKeys.Pg,
      useClass: PgService,
    },
    /**
     * Redis
     */
    {
      provide: ProviderKeys.Redis,
      useClass: RedisService,
    },
    /**
     * Http
     */
    {
      provide: ProviderKeys.Http,
      useClass: Http,
    },
  ],
})
export class CaptainAmericaModule { }
