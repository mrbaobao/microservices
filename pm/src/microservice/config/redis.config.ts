const RedisConfig = {
    host: process.env.REDIS_HOST,
    port: +(process.env.REDIS_PORT ?? 5432),
    maxClients: +(process.env.REDIS_MAX_CLIENTS ?? 100),
    path: process.env.REDIS_PATH ?? null,
}
//Export
export { RedisConfig }