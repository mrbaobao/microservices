/**
 * Service Config
 */
const ServiceConfig = {
    appKey: process.env.APP_KEY,
    name: process.env.PM_NAME,
    pm: process.env.PM_NAME,
    apiPort: +process.env.PM_PORT,
    requestTimeout: +process.env.REQUEST_TIMEOUT,
}
//Export
export { ServiceConfig }