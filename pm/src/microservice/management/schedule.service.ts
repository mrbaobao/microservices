import { Injectable } from "@nestjs/common";
import { PgService } from "../lib/pg/pg.service";
import { RedisService } from "../lib/redis/redis.service";
import { Schedule } from "./types/schedule.type";

@Injectable()
class ScheduleService {
    private readonly redis = new RedisService()
    private readonly pg = new PgService()
    /**
     * Get a schedule by name
     * @param name 
     * @returns 
     */
    get = async (key: string): Promise<Schedule | null> => {
        /**
                 * Insert to database
                 */
        const sql = `
            SELECT data 
            FROM schedules
            WHERE key = $1
        `

        const params = [
            key,
        ]
        //Query
        const res = await this.pg.query(sql, params)
        //Return data
        const schedule = res.rows[0]["data"] ?? null
        console.log(9999111, "Schdeulung from db ", schedule)
        return schedule
    }
}
//Export
export { ScheduleService }