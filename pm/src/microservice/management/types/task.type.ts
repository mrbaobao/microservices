import { ValidateType } from "../validator"

//All requirements
type Requirements = { [service_action: string]: "success" | "failed" }

//Task
interface Task {
    /**
     * Next tasks
     * "exit" | "next" | "throw"
     * Default: "throw" --> throw error to Schedule
     */
    on_error?: "exit" | "next" | "throw"
    //Restart count limit: only works when task failed
    restart_attempts?: number
    /**
     * Require service actions
     * Ex:
     * {
     *      "heros/create-mission": "success", //Must executed & success
     *      "heros/remove-mission": "failed", //Must executed & failed
     * }
     */
    require?: Requirements | Requirements[]
    /**
     * Input schema
     * Ex: {
     *      "id": "string",
     *      "name": "string_optional",
     *      "limit": "integer+",
     * }
     */
    input_schema?: { [field: string]: ValidateType }
}
//Export
export { Task, Requirements }
