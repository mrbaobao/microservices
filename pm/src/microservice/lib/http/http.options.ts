import { ServiceConfig } from "../../config/service.config";
const HttpOptions = {
    timeout: 1000,
    headers: {
        'app-key': ServiceConfig.appKey,
    }
}
//Export
export { HttpOptions }