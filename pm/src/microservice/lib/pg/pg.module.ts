import { Module } from "@nestjs/common";
import { PgService } from "./pg.service";

@Module({
    exports: [
        PgService
    ]
})
export class PgModule { }