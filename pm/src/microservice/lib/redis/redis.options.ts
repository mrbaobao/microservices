import { RedisConfig } from "../../config/redis.config"
import { CacheServiceOptions } from "./redis.types"

const RedisOptions: CacheServiceOptions = {
    maxClients: RedisConfig.maxClients,
    socket: {
        host: RedisConfig.host,
        port: RedisConfig.port,
        path: RedisConfig.path,
    },
}
//Export
export { RedisOptions }