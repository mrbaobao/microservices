import { RedisClientType } from "redis4"
import { RedisService } from "./redis.service"

interface CacheNode {
    //Get key
    get key(): string
    //To string
    toString: () => string
}
type StackInfo = { key: string, status: number }[]
class CacheStack<T extends CacheNode> {
    //Cache
    private readonly cache = new RedisService()
    //From string to T
    private fromString: (jsonString: string) => T
    //Key
    private key: string
    private size: number

    /**
     * Construct with size
     */
    constructor({ size, key, fromString }:
        {
            size?: number, key: string,
            fromString: (jsonString: string) => T,
        }) {
        this.size = size ?? Number.MAX_VALUE
        this.key = key
        this.fromString = fromString
    }
    //Cache key
    private getCacheKey = (nodeKey: string, groupKey: string) => this.key + ":" + groupKey + ":" + nodeKey
    //Group key
    private getCacheGroupKey = (groupKey: string) => this.key + ":" + groupKey
    /**
     * Push all
     * @param groupKey 
     * @returns 
     */
    addAll = async ({ nodes, groupKey, redisClient, status }:
        {
            nodes: T[], groupKey: string,
            redisClient?: RedisClientType,
            status?: number
        }) => {
        status = status ?? 0
        const count = nodes.length
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(redisClient, async redisClient => {
            const redisMulti = redisClient.multi()
            let keys: StackInfo = []
            if (await redisClient.exists(groupCacheKey)) {
                const jsonString = await redisClient.get(groupCacheKey)
                keys = JSON.parse(jsonString)
            }
            //Loop
            for (let index = count - 1; index >= 0; index--) {
                const node = nodes[index]
                //Push
                keys = await this._add(node, groupKey, status, keys, redisMulti)
            }
            //Update info
            redisMulti.set(groupCacheKey, JSON.stringify(keys))

            /**
             * End transaction
             */
            await redisMulti.exec()
        })
    }
    /**
     * Push new node
     * 1. Add last
     * 2. Remove first if count exceed size!
     * @param node
     * @returns 
     */
    add = async ({ node, groupKey, status, redisClient }:
        {
            node: T, groupKey: string,
            status?: number,
            redisClient?: RedisClientType
        }) => {
        status = status ?? 0
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        /**
         * Start cache
         */
        return await this.cache.start(redisClient, async redisClient => {
            //Stack info
            let keys: StackInfo = []
            if (await redisClient.exists(groupCacheKey)) {
                const jsonString = await redisClient.get(groupCacheKey)
                keys = JSON.parse(jsonString)
            }
            const redisMulti = redisClient.multi()
            //Push
            keys = await this._add(node, groupKey, status, keys, redisMulti)
            //Update info
            redisMulti.set(groupCacheKey, JSON.stringify(keys))
            //Exec
            await redisMulti.exec()
            /**
             * Return stack info
             */
            return keys
        })
    }
    /**
     * Insert last
     * @param param0 
     * @returns 
     */
    push = async ({ node, groupKey, status, redisClient }:
        {
            node: T, groupKey: string,
            status?: number,
            redisClient?: RedisClientType
        }) => {
        status = status ?? 0
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        /**
         * Start cache
         */
        return await this.cache.start(redisClient, async redisClient => {
            //Stack info
            let keys: StackInfo = []
            if (await redisClient.exists(groupCacheKey)) {
                const jsonString = await redisClient.get(groupCacheKey)
                keys = JSON.parse(jsonString)
            }
            if (keys.length >= this.size) {
                return keys
            }
            const redisMulti = redisClient.multi()
            //Push
            keys = await this._push(node, groupKey, status, keys, redisMulti)
            //Update info
            redisMulti.set(groupCacheKey, JSON.stringify(keys))
            //Exec
            await redisMulti.exec()
            /**
             * Return stack info
             */
            return keys
        })
    }
    /**
     * Update node
     * @param node
     * @returns 
     */
    update = async ({ node, groupKey }: { node: T, groupKey: string }) => {
        const cacheKey = this.getCacheKey(node.key, groupKey)
        return await this.cache.start(async redisClient => {
            //Exists --> Replace
            if (await redisClient.exists(cacheKey)) {
                //Update cache
                await redisClient.set(cacheKey, node.toString())
            }
        })
    }
    /**
     * Update status only
     * - Continuosly from 0 to count -1 
     * @param param0 
     * @returns 
     */
    updateStatus = async ({ groupKey, status, count }:
        {
            count: number,
            groupKey: string,
            status: number
        }) => {
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(async redisClient => {
            if (await redisClient.exists(groupCacheKey)) {
                /**
                 * Stack info
                 */
                const jsonString = await redisClient.get(groupCacheKey)
                const keys: StackInfo = JSON.parse(jsonString)
                const keysCount = keys.length
                if (count > keysCount) {
                    count = keysCount
                }
                /**
                 * Update info
                 */
                for (let index = 0; index < count; index++) {
                    keys[index].status = status
                }
                /**
                 * Update cache
                 */
                redisClient.set(groupCacheKey, JSON.stringify(keys))
            }
        })
    }
    /**
     * Delete cache
     * @param key 
     * @param groupKey 
     * @returns 
     */
    delete = async ({ key, groupKey, redisClient }:
        { key: string, groupKey: string, redisClient?: RedisClientType }): Promise<T> => {
        const cacheKey = this.getCacheKey(key, groupKey)
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(redisClient, async redisClient => {
            let keys: StackInfo = []
            if (await redisClient.exists(groupCacheKey)) {
                const jsonString = await redisClient.get(groupCacheKey)
                keys = JSON.parse(jsonString)
                //Exists --> Delete
                const index = keys.findIndex(info => info.key === key)
                if (index >= 0) {
                    const redisMulti = redisClient.multi()
                    //Get node to return
                    const node = await this.getNode(key, groupKey, redisClient)
                    //Delete cache
                    redisMulti.del(cacheKey)
                    //Delete key
                    keys.splice(index, 1)
                    //Update keys
                    redisMulti.set(groupCacheKey, JSON.stringify(keys))
                    //Exec
                    await redisMulti.exec()
                    //Return key
                    return node
                }
            }
            //False
            return null
        })
    }
    /**
     * Push new node
     * 1. Add last
     * 2. Remove first if count exceed size!
     * @param node
     * @returns 
     */
    private _add = async (
        node: T, groupKey: string,
        status: number,
        keys: StackInfo,
        redisMulti: any) => {
        //Push key
        keys.unshift({ key: node.key, status })

        /**
         * Add to cache
         */
        await this.addNode(node, groupKey, redisMulti)
        //Count
        const count = keys.length

        /**
         * Check count exceed size
         */
        if (count > this.size) {
            //Remove last
            await this.removeNode(keys[count - 1].key, groupKey, redisMulti)
            //Remove key
            keys.pop()
        }
        //Return keys
        return keys
    }
    private _push = async (
        node: T, groupKey: string,
        status: number,
        keys: StackInfo,
        redisMulti: any) => {
        //Push key
        keys.push({ key: node.key, status })
        /**
         * Add to cache
         */
        await this.addNode(node, groupKey, redisMulti)
        //Return keys
        return keys
    }
    /**
     * Convert to array
     */
    changeStatusAll = async ({ groupKey, status, newStatus, callback }:
        {
            groupKey: string,
            status: number,
            newStatus: number,
            callback: (node: T) => any
        }): Promise<void> => {
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(async redisClient => {

            const isExists = await redisClient.exists(groupCacheKey)
            if (!isExists) {
                return
            }
            const jsonString = await redisClient.get(groupCacheKey)
            //Stack keys
            let keys: StackInfo = JSON.parse(jsonString)
            const count = keys.length
            if (count === 0) {
                return
            }
            let node: T
            for (let index = 0; index < count; index++) {
                node = await this.getNode(keys[index].key, groupKey, redisClient)
                if (keys[index].status === status) {
                    keys[index].status = newStatus
                    //Callback
                    callback(node)
                } else {
                    //Update info
                    await redisClient.set(groupCacheKey, JSON.stringify(keys))
                    //Stop
                    return
                }
            }
        })
    }
    getAll = async ({ groupKey, limit }: { groupKey: string, limit: number }): Promise<T[]> => {
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(async redisClient => {

            const isExists = await redisClient.exists(groupCacheKey)
            if (!isExists) {
                return null
            }
            const jsonString = await redisClient.get(groupCacheKey)
            //Stack keys
            let keys: StackInfo = JSON.parse(jsonString)
            const count = keys.length
            if (count === 0) {
                return []
            }
            const arr: T[] = []
            let node: T
            if (limit > count) {
                limit = count
            }
            for (let index = 0; index < limit; index++) {
                node = await this.getNode(keys[index].key, groupKey, redisClient)
                arr.push(node)
            }
            //Return
            return arr
        })
    }
    /**
     * First element of stack
     * @param param0 
     * @returns 
     */
    first = async ({ groupKey, redisClient }: { groupKey: string, redisClient?: RedisClientType }): Promise<T> => {
        return await this.getNodeAtIndex({ groupKey, redisClient, index: 0 })
    }
    last = async ({ groupKey, redisClient }: { groupKey: string, redisClient?: RedisClientType }): Promise<T> => {
        return await this.getNodeAtIndex({ groupKey, redisClient, index: -1 })
    }
    /**
     * Get
     */
    node = async ({ groupKey, key }): Promise<T> => {
        const cacheKey = this.getCacheKey(key, groupKey)
        return await this.cache.start(async redisClient => {
            if (await redisClient.exists(cacheKey)) {
                const jsonString = await redisClient.get(cacheKey)
                return this.fromString(jsonString)
            }
            //Not found
            return null
        })

    }
    private getNodeAtIndex = async ({ index, groupKey, redisClient }:
        { index: number, groupKey: string, redisClient?: RedisClientType }): Promise<T> => {
        const groupCacheKey = this.getCacheGroupKey(groupKey)
        return await this.cache.start(redisClient, async redisClient => {

            const isExists = await redisClient.exists(groupCacheKey)
            if (!isExists) {
                return null
            }
            const jsonString = await redisClient.get(groupCacheKey)
            //Stack keys
            let keys: StackInfo = JSON.parse(jsonString)
            const count = keys.length
            if (count === 0) {
                return null
            }
            if (index < 0) {
                index = count + index
            }
            //Return
            return await this.getNode(keys[index].key, groupKey, redisClient)
        })
    }
    /**
     * Get
     */
    private getNode = async (key: string, groupKey: string, redisClient: RedisClientType): Promise<T> => {
        const cacheKey = this.getCacheKey(key, groupKey)
        if (await redisClient.exists(cacheKey)) {
            const jsonString = await redisClient.get(cacheKey)
            return this.fromString(jsonString)
        }
    }
    /**
     * Add
     */
    private addNode = (node: T, groupKey: string, redisMulti: any) => {
        return redisMulti.set(this.getCacheKey(node.key, groupKey), node.toString())
    }
    /**
     * Remove
     */
    private removeNode = (key: string, groupKey: string, redisMulti: any) => {
        return redisMulti.del(this.getCacheKey(key, groupKey))
    }
}
//Export
export { CacheStack }
export type { CacheNode, StackInfo }