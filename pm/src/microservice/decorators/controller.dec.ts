import * as Nest from "@nestjs/common"
import { MessagePattern } from "@nestjs/microservices"
import { ServiceConfig } from "../config/service.config"
import { DECOR_KEYS_REGISTER, DECOR_KEYS_REQUEST, DECOR_KEYS_TASK, DECOR_KEYS_TASK_RESPOND } from "./constants"
import { copyMetadata } from "./copy-metadata"

/**
 * Microservice name
 */
const Service = ServiceConfig.name
/**
 * Method needs respond
 */
function AddRespond() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Original method
        const childFunction = descriptor.value
        /**
         * Custom method
         */
        descriptor.value = async function (...params: any[]) {
            const mid = params[0]?.mid
            const data = await childFunction.apply(this, params)
            if (mid) {
                this.broker?.respond(mid, data)
            }
        }
        //Return new descriptor
        return descriptor
    }
}
/**
 * Method needs respond from register
 */
function AddRegisterRespond() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Original method
        const childFunction = descriptor.value
        /**
         * Custom method
         */
        descriptor.value = async function (...params: any[]) {
            const mid = params[0]?.mid
            const data = await childFunction.apply(this, params)
            if (mid) {
                this.broker?.registerRespond(mid, data)
            }
        }
        //Return new descriptor
        return descriptor
    }
}
/**
 * TaskRespond
 */
function TaskRespond(from: string, event: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Original method
        const childFunction = descriptor.value
        /**
         * Custom method
         */
        descriptor.value = async function (...params: any[]) {
            const data = await childFunction.apply(this, params)
            const { pid } = params[0]
            this.broker?.taskRespond(from, event, {
                ...data, pid
            })
        }
        //Return new descriptor
        return descriptor
    }
}
/**
 * Controller
 */
const Controller = () => {
    return function (target: any) {
        const descriptors = Object.getOwnPropertyDescriptors(target.prototype);
        for (const [propertyKey, descriptor] of Object.entries(descriptors)) {
            const isMethod =
                typeof descriptor.value == 'function' &&
                propertyKey != 'constructor';
            //Only for method
            if (!isMethod) continue;
            const originalMethod = descriptor.value;
            /**
             * Task Respond
             * Service: pm
             * task_respond:[pm]:[hero]:[event]
             */
            const taskRespondPattern: string = Reflect.getMetadata(`${DECOR_KEYS_TASK_RESPOND}:${propertyKey}`, target.prototype)
            if (taskRespondPattern) {
                const [service, event] = taskRespondPattern.split(":")
                //MessagePattern: task_respond:[RemoteService]:[ThisService]:[event]
                MessagePattern(`task_respond:${Service}:${service}:${event}`)(target, propertyKey, descriptor)
            }
            /**
             * Task
             * Service: hero
             * task:[pm]:[hero]:[event]
             */
            const taskPattern: string = Reflect.getMetadata(`${DECOR_KEYS_TASK}:${propertyKey}`, target.prototype)
            if (taskPattern) {
                const [from, event] = taskPattern.split(":")
                /**
                 * Respond
                 */
                TaskRespond(from, event)(target, propertyKey, descriptor)
                //MessagePattern
                MessagePattern(`task:${from}:${Service}:${event}`)(target, propertyKey, descriptor)
            }
            /**
             * Request-Respond
             * Service: pm
             * request:[pm]:[hero]:[event]
             */
            const pattern = Reflect.getMetadata(`${DECOR_KEYS_REQUEST}:${propertyKey}`, target.prototype)
            if (pattern) {
                const [to, event] = pattern.split(":")
                /**
                 * Respond
                 */
                AddRespond()(target, propertyKey, descriptor)
                //MessagePattern
                MessagePattern(`request:${Service}:${to}:${event}`)(target, propertyKey, descriptor)
            }
            /**
             * Register
             */
            const registerPattern = Reflect.getMetadata(`${DECOR_KEYS_REGISTER}:${propertyKey}`, target.prototype)
            if (registerPattern) {
                const [from, event] = registerPattern.split(":")
                /**
                 * Respond
                 */
                AddRegisterRespond()(target, propertyKey, descriptor)
                //MessagePattern
                MessagePattern(`register:${from}:${Service}:${event}`)(target, propertyKey, descriptor)
            }

            //Copy MetaData
            if (originalMethod != descriptor.value) {
                copyMetadata(originalMethod, descriptor.value);
            }
            /**
             * Update
             */
            Object.defineProperty(target.prototype, propertyKey, descriptor);
        }
        //Add @nestjs Controller
        Nest.Controller()(target)
        //Return target
        return target
    }
}
/**
 * Export
 */
export { Controller }