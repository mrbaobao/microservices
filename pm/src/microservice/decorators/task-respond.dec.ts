import { DECOR_KEYS_TASK_RESPOND } from "./constants"

/**
 * Method decorator
 * Task respond from service
 * Note:
 *  <target> in MethodDecorator = <target.prototype> in ClassDecorator
 */
function TaskResponsePattern(pattern: any) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //Define metadata
        Reflect.defineMetadata(`${DECOR_KEYS_TASK_RESPOND}:${propertyKey}`, pattern, target)
        //Return descriptor
        return descriptor
    }
}
/**
 * Export
 */
export { TaskResponsePattern } 