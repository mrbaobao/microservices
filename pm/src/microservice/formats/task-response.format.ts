const TaskRespond = {
    //Success
    success: (data: any) => {
        return {
            status: "success",
            data,
        }
    },
    //Failed
    failed: (error: any) => {
        return {
            status: "failed",
            error,
        }
    }
}
//Export
export { TaskRespond }