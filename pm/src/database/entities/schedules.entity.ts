import { Column, Entity, PrimaryColumn } from "typeorm"
@Entity({
    name: "schedules",
})
export class Schedule {
    @PrimaryColumn({
        type: "character varying",
        length: 50,
        nullable: false,
    })
    key: string;

    @Column({
        type: "jsonb",
    })
    data: any;

}