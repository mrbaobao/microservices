import { DataSource } from "typeorm";
import { createDatabase } from "typeorm-extension";
import ormconfig from './ormconfig';
import { ScheduleSample } from "./sample-data/schedule";
/**
 * Constants
 */
const Colors = {
    clearColor: "\u001b[1;0m",
    red: "\u001b[1;31m",
    green: "\u001b[1;32m",
    yellow: "\u001b[1;33m",
}

/**
 * Add a sample schedule
 * @param dataSource 
 * @returns 
 */
const addSampleSchedule = (dataSource: DataSource) => {
    const sql = `
        INSERT INTO schedules
            (key, data)
        VALUES
            ($1, $2)
        ON CONFLICT (key)
        DO NOTHING
    `
    const params = [
        ScheduleSample.name,
        ScheduleSample
    ]
    //Query
    return dataSource.query(sql, params)
}
const initDB = () => {
    console.log(
        `${Colors.green}InitDB...${Colors.clearColor}`
    );
    /**
     * Create database if not existed
     */
    createDatabase({
        options: ormconfig,
        ifNotExist: true,
    })
        .then(() => {
            /**
             * Initialize database
             */
            const dataSource = new DataSource(ormconfig)
            dataSource.initialize()
                .then(() => {
                    console.log(
                        `${Colors.green}Add sample data...${Colors.clearColor}`
                    );
                    //Add sample data
                    addSampleSchedule(dataSource)
                })
                .catch((e) => {
                    console.log(
                        `${Colors.red}Initialize database failed...${Colors.clearColor}`
                    );
                })
        })
        .catch((e) => {
            console.log("InitDB: Create database error", e)
        })

}
//Export
export { initDB }