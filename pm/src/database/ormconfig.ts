import * as path from "path";
import { DataSourceOptions } from "typeorm";

export default {
  type: "postgres",
  host: process.env.PG_HOST,
  port: +process.env.PG_PORT,
  username: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DATABASE,
  synchronize: true,
  migrationsRun: true,
  dropSchema: true,
  entities: [
    path.join(__dirname, ".", "entities", "**", "*.*"),
    path.join(__dirname, ".", "entities", "*.*")
  ],
  migrations: [
    path.join(__dirname, "migrations", "*.*")
  ],
  cli: {
    entitiesDir: path.join(__dirname, "..", "entities"),
    migrationsDir: path.join(__dirname, "migrations")
  }
} as DataSourceOptions
