const ScheduleSample = {
    "name": "mission-impossible-1",
    "on_error": "exit",
    "restart_attempts": 1,
    "tasks": [{
        "hero:move-to-moon": {
            "on_error": "next",
            "input_schema": {
                "name": "string_not_empty"
            },
            "restart_attempts": 2
        }
    },
    {
        "hero:move-to-mars": {
            "require": {
                "hero:move-to-moon": "failed"
            },
            "on_error": "next",
            "input_schema": {
                "name": "string_not_empty"
            }
        }
    },
    {
        "hero:check-aliens": {
            "require": [{
                "hero:move-to-moon": "success"
            },
            {
                "hero:move-to-mars": "success"
            }
            ],
            "input_schema": {
                "aliens": "boolean"
            }
        },
        "hero:check-survival-info": {
            "require": [{
                "hero:move-to-moon": "success"
            },
            {
                "hero:move-to-mars": "success"
            }
            ],
            "input_schema": {
                "survival": "number"
            }
        }
    }
    ]
}

//Export
export { ScheduleSample }