import { CaptainAmericaModule } from './pm.module';
import { bootstrap } from './microservice';
import { initDB } from './database/init-db';

/**
 * Init db
 */
if (process.env.PG_INIT === 'true') {
    initDB()
}

/**
 * Start
 */
bootstrap(CaptainAmericaModule)
